import time
import requests
import json
import random

class Requesturl:
    def __init__(self):
        self.bot = requests.session()
        self.proxies = self.changeProxy()

    def changeProxy(self):
        with open('/tmp/rternewproxy.json') as f:
            data = json.loads(f.read())
            proxies = random.choice(data)
        
        return proxies

    def closeSession(self):
        self.bot.close()

    def request(self, url, rjson=False):
        for i in range(5):
            try:
                r = self.bot.get(url, timeout=180, proxies=self.proxies)
                if r.ok:
                    if rjson:
                        urlcontent = r.json()
                    else:
                        urlcontent = r.content

                    return(urlcontent)
                else:
                    return
                    
            # 若發生例外則更換proxy並重新執行一次
            except Exception as err:
                print(f'Retry: {err} {i} {url}')
                self.proxies = self.changeProxy()
                time.sleep(5)

