#!/usr/bin/env python3
import json
from requesturl import Requesturl
from bs4 import BeautifulSoup


class Itn:

    def __init__(self):
        self.baseurl = 'https://news.ltn.com.tw/ajax/breakingnews/all/'
        self.rq = Requesturl()

    def getContent(self):
        page = 0
        result = []
        while True:
            page += 1
            # 取得ajax資料
            doc = self.rq.request(f'{self.baseurl}{page}', True)
            # 若除了200外 則下一頁
            if doc == None:
                next
            else:
                # 若有資料則取得 沒資料則回傳陣列
                if doc['data']:
                    # 判斷資料型態為list 或 dict
                    if isinstance(doc['data'], list):
                        for value in doc['data']:
                            desc = str()
                            it = self.rq.request(value['url'])
                            for t in BeautifulSoup(it, "html.parser").select('div.text p:not([class])'):
                                desc += t.text

                            content = {
                                "provide": str(value['url']).split(".")[1],
                                "key": value['no'],
                                'title': value['title'],
                                'pdt': BeautifulSoup(it, "html.parser").select_one('span.time').text.strip(),
                                'url': value['url'],
                                'image': value['photo_L'],
                                'desc': desc
                            }
                            result.append(content)
                    else:
                        for key, value in doc['data'].items():
                            desc = str()
                            it = self.rq.request(value['url'])
                            for t in BeautifulSoup(it, "html.parser").select('div.text p:not([class])'):
                                desc += t.text

                            content = {
                                "provide": str(value['url']).split(".")[1],
                                "key": value['no'],
                                'title': value['title'],
                                'pdt': BeautifulSoup(it, "html.parser").select_one('span.time').text,
                                'url': value['url'],
                                'image': value['photo_L'],
                                'desc': desc
                            }
                            result.append(content)

                else:
                    return(result)

    def start(self):
        # 獲得網址內容
        result = []
        result = self.getContent()

        # 結算&轉型態
        print(f"共{len(result)}筆資料")
        result = (json.dumps(result, indent=4, ensure_ascii=False))

        # 輸出json檔
        with open('./json/itn.json', 'w') as f:
            f.write(result)

        # 關閉Session
        self.rq.closeSession()


if __name__ == '__main__':
    itn = Itn()
    itn.start()
