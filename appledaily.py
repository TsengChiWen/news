#!/usr/bin/env python3
import json
from requesturl import Requesturl
from bs4 import BeautifulSoup


class Appledaily:

    def __init__(self):
        self.page = 20
        self.rq = Requesturl()

    def getUrl(self):
        items = {}

        # 動新聞網址
        while True:
            u = self.rq.request(
                f"https://tw.appledaily.com/pf/api/v3/content/fetch/query-feed?query=%7B%22feedOffset%22%3A0%2C%22feedQuery%22%3A%22taxonomy.tags.text.raw%253Atheme_videowall%2520AND%2520type%253Astory%252BAND%252Bdisplay_date%253A%255B*%2520TO%2520now%255D%252BAND%252B_exists_%253Apromo_items.basic.streams.url%22%2C%22feedSize%22%3A{self.page}%2C%22sort%22%3A%22display_date%3Adesc%22%7D&d=188&_website=tw-appledaily", True)
            if u == None:
                break
            elif u['content_elements']:
                for i in u['content_elements']:
                    items[i['_id']
                          ] = f"https://tw.appledaily.com{i['website_url']}"
                self.page += 20
            
        # 即時新聞網址
        u = self.rq.request(f"https://tw.appledaily.com/pf/api/v3/content/fetch/query-feed?query=%7B%22feedOffset%22%3A0%2C%22feedQuery%22%3A%22type%253Astory%2520AND%2520taxonomy.primary_section._id%253A%252F%255C%252Frealtime.*%252F%22%2C%22feedSize%22%3A%22100%22%2C%22sort%22%3A%22display_date%3Adesc%22%7D&d=188&_website=tw-appledaily", True)
        if u == None:
            return
        else:
            # 取得即時新聞網址
            for i in u['content_elements']:
                items[i['_id']
                      ] = f"https://tw.appledaily.com{i['website_url']}"
        
        u = self.rq.request('https://tw.appledaily.com/arcio/news-sitemap/')
        if u == None:
            return
        else:
            # 取得新聞網址
            for t in BeautifulSoup(u, "html.parser").find_all('loc'):
                items[str(t.text).split('/')[5]] = t.text
        return items

    def getContent(self, it, key):
        item = self.rq.request(it)
        if item == None:
            return
        else:
            bs = BeautifulSoup(item, "html.parser")
            # 取得JS內容
            data = json.loads(str(bs.select_one('script[type="application/ld+json"]'))
                  .replace('<script type="application/ld+json">', "").replace('</script>', ""))

            # 處理內容文字
            desc = str()
            for i in bs.find_all('p'):
                desc += (i.text).replace("\u3000", " ")

            # 整理成JSON格式數據
            result = {
                "provide": data['image']['siteProperties']['siteName'],
                "key": key,
                'title': data['headline'],
                'pdt': data['datePublished'],
                'url': it,
                'image': data['image']['url'],
                'desc': desc
            }
            return(result)

    def start(self):

        # 獲得網址內容
        result = []
        itemsUrl = self.getUrl()

        # 若有新聞連結且筆數>0 則搜尋新聞內容
        if not itemsUrl == None and len(itemsUrl) > 0:
            for key, value in itemsUrl.items():
                result.append(self.getContent(value, key))

        # 結算&轉型態
        print(f"共{len(result)}筆資料")
        result = (json.dumps(result, indent=4, ensure_ascii=False))

        # 輸出json檔
        with open('./json/appledaily.json', 'w') as f:
            f.write(result)

        # 關閉Session
        self.rq.closeSession()


if __name__ == '__main__':
    ap = Appledaily()
    ap.start()
