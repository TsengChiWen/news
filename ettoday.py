#!/usr/bin/env python3
import json
from requesturl import Requesturl
from bs4 import BeautifulSoup


class Ettoday:

    def __init__(self):
        self.baseurl = 'https://www.ettoday.net/news/news-list.htm'
        self.rq = Requesturl()

    def getUrl(self, url):
        items = []
        doc = self.rq.request(url)
        if doc == None:
            return
        else:
            bs = BeautifulSoup(doc, "html.parser")
            # 取得新聞網址
            for i in bs.select('div.part_list_2 h3 a'):
                items.append(f"https://www.ettoday.net/{i['href']}")
        return items

    def getContent(self, it):
        item = self.rq.request(it)
        if item == None:
            return
        else:
            bs = BeautifulSoup(item, "html.parser")
            # 處理內容文字
            desc = str()
            media = []
            for t in bs.select('div.story p:not([class])'):
                desc += t.text

            # 處理圖片連結
            if bs.select('div.story p img'):
                for i in bs.select('div.story p img'):
                    media.append(f"https:{i['src']}")
                    
            elif bs.select('.css-9pa8cd'):
                for i in bs.select('.css-9pa8cd'):
                    media.append(f"https:{i['src']}")

            elif bs.select_one('div.story p iframe'):
                media.append(bs.select_one('div.story p iframe')['src'])

            

            # 整理成JSON格式數據
            result = {
                "provide": str(it).split('.')[1],
                "key": str(it).split('/')[-1].split('.')[0],
                'title': bs.select_one('h1').text,
                'pdt': bs.select_one('time').text.strip(),
                'url': it,
                'imgs': media,
                'desc': desc
            }
            return(result)

    def start(self):
        # 獲得網址內容
        result = []
        itemsUrl = self.getUrl(self.baseurl)

        # 若有新聞連結且筆數>0 則搜尋新聞內容
        if not itemsUrl == None and len(itemsUrl) > 0:
            for value in itemsUrl:
                result.append(self.getContent(value))

        # 結算&轉型態
        print(f"共{len(result)}筆資料")
        result = (json.dumps(result, indent=4, ensure_ascii=False))

        # 輸出json檔
        with open('./json/ettoday.json', 'w') as f:
            f.write(result)

        # 關閉Session
        self.rq.closeSession()


if __name__ == '__main__':
    et = Ettoday()
    et.start()
