#!/usr/bin/env python3
import json
from requesturl import Requesturl
from bs4 import BeautifulSoup
import re


class Yahoo:

    def __init__(self):
        self.baseurl = 'https://tw.news.yahoo.com/'
        self.rq = Requesturl()

    # 取得新聞類別
    def getUrl(self):
        u = self.rq.request(self.baseurl)
        url = []
        for a in BeautifulSoup(u, "html.parser").select('div.ybar-mod-navigation.ybar-shift-more-menu > ul > li > a'):
            url.append(a['href'])
        return url

    # 取得各新聞url
    def getItems(self, url):
        item = self.rq.request(url)
        itemsUrl = []
        bs = BeautifulSoup(item, "html.parser")
        if bs.select('div.Cf div h3 a'):
            for u in bs.select('div.Cf div h3 a'):
                itemsUrl.append(f"https://tw.news.yahoo.com{u['href']}")
        return itemsUrl

    # 取得新聞內容
    def getContent(self, url):
        result = []
        desc = str()
        # 取得ajax資料
        doc = self.rq.request(url)
        # 若除了200外 則下一頁
        if doc == None:
            next
        else:
            bs = BeautifulSoup(doc, "html.parser")
            if bs.select_one('script[type="application/ld+json"]'):
                data = json.loads(
                    str(bs.select_one('script[type="application/ld+json"]'))
                    .split('<script type="application/ld+json">')[1]
                    .split('</script>')[0], strict=False
                )
                for t in range((len(bs.select('.caas-body p')))-3):
                    desc += bs.select('.caas-body p')[t].text

                content = {
                    "provide": data['creator']['name'],
                    "key": re.search("-(\w+).html", url).group(1),
                    'title': data['headline'],
                    'pdt': data['dateModified'],
                    'url': data['mainEntityOfPage'],
                    'image': data['image']['url'],
                    'desc': desc
                }
                result.append(content)
            return result

    def start(self):
        # 獲得網址內容
        result = []
        url = self.getUrl()
        for u in url:
            for i in self.getItems(u):
                result.append(self.getContent(i))

        # 結算&轉型態
        print(f"共{len(result)}筆資料")
        result = (json.dumps(result, indent=4, ensure_ascii=False))

        # 輸出json檔
        with open('./json/yahoo.json', 'w') as f:
            f.write(result)

        # 關閉Session
        self.rq.closeSession()


if __name__ == '__main__':
    yahoo = Yahoo()
    yahoo.start()
