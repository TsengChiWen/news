#!/usr/bin/env python3
import json
from requesturl import Requesturl
from bs4 import BeautifulSoup
from functools import reduce

class Tvbs:

    def __init__(self):
        self.baseurl = 'https://news.tvbs.com.tw/'
        self.rq = Requesturl()

    def getUrl(self):
        u = self.rq.request(self.baseurl)
        url = []
        # 取得新聞類別
        for a in BeautifulSoup(u, "html.parser").select('div.op > div.header_menu > div.header_menu_bar > div.padding20_mo > div.main.nav_1366 > nav > ul > li > a#first_cate'):
            url.append(a['href'])

        return url

    def getItems(self, url):
        item = self.rq.request(url)
        itemsUrl = []
        bs = BeautifulSoup(item, "html.parser")
        # 即時新聞格式
        if bs.select('#realtime_data li a'):
            for u in bs.select('#realtime_data li a'):
                itemsUrl.append(f"https://news.tvbs.com.tw{u['href']}")
        # 其他新聞格式
        elif bs.select('#block_pc li a'):
            for u in bs.select('#block_pc li a'):
                itemsUrl.append(f"https://news.tvbs.com.tw{u['href']}")
        # 直播格式
        else:
            for u in bs.select('.live_list_box a'):
                itemsUrl.append(u['href'])

        return itemsUrl

    def getContent(self, url):
        result = []
        # 取得ajax資料
        doc = self.rq.request(url)
        # 若除了200外 則下一頁
        if doc == None:
            next
        else:
            bs = BeautifulSoup(doc, "html.parser")
            if bs.select_one('script[type="application/ld+json"]'):
                data = json.loads(
                    str(bs.select_one('script[type="application/ld+json"]'))
                    .split('<script type="application/ld+json">')[1]
                    .split('</script>')[0], strict=False
                )
                if 'articleBody' in data:
                    content = {
                        "provide": data['publisher']['name'],
                        "key": str(data['mainEntityOfPage']).split('/')[-1],
                        'title': data['headline'],
                        'pdt': data['dateCreated'],
                        'url': data['mainEntityOfPage'],
                        'image': data['image']['url'],
                        'desc': data['articleBody']
                    }
                    result.append(content)
                else:
                    content = {
                        "provide": data['author']['name'],
                        "key": str(data['mainEntityOfPage']).split('/')[-1],
                        'title': data['headline'],
                        'pdt': data['dateCreated'],
                        'url': data['mainEntityOfPage'],
                        'image': data['image']['url'],
                        'desc': "直播新聞"
                    }
                    result.append(content)
            result = reduce(lambda x, y: x if y in x else x + [y], [[], ] + result)
            return result

    def start(self):
        # # 獲得網址內容
        result=[]
        url = self.getUrl()
        for u in url:
            for i in self.getItems(u):
                result.append(self.getContent(i))

        # 結算&轉型態
        print(f"共{len(result)}筆資料")
        result = (json.dumps(result, indent=4, ensure_ascii=False))
        
        #輸出json檔
        with open('./json/tvbs.json', 'w') as f:
            f.write(result)

        # 關閉Session
        self.rq.closeSession()

if __name__ == '__main__':
    tvbs=Tvbs()
    tvbs.start()
