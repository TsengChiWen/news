from appledaily import Appledaily
from ettoday import Ettoday
from itn import Itn
from tvbs import Tvbs
from yahoo import Yahoo

if __name__ == '__main__':
    apple = Appledaily()
    apple.start()
    etoday = Ettoday()
    etoday.start()
    itn = Itn()
    itn.start()
    tvbs = Tvbs()
    tvbs.start()
    yahoo = Yahoo()
    yahoo.start()

